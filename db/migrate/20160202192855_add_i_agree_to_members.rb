class AddIAgreeToMembers < ActiveRecord::Migration
  def change
    add_column :members, :i_agree, :boolean
  end
end
