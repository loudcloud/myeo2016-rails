class CreateMembers < ActiveRecord::Migration
  def change
    create_table :members do |t|
      t.string :last_name
      t.string :first_name
      t.string :member_number
      t.string :email
      t.string :contact_number
      t.string :payment_option

      t.timestamps null: false
    end
  end
end
