class CreateParticipants < ActiveRecord::Migration
  def change
    create_table :participants do |t|
      t.references :member, index: true, foreign_key: true
      t.string :last_name
      t.string :first_name
      t.string :nickname
      t.string :birthdate
      t.string :email
      t.text :hobbies
      t.string :shirt_size
      t.text :medical_condition
      t.text :diet

      t.timestamps null: false
    end
  end
end
