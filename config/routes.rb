Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".
  root 'pages#index'
  get 'itinerary' => 'pages#itinerary'
  get 'faq' => 'pages#faq'
  get 'reminders' => 'pages#reminders'
  get 'contact' => 'pages#contact'
  get 'register' => 'pages#contact'
  post 'do_register' => 'pages#do_register'
end
