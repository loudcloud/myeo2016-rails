# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'myeo2016-rails'
set :repo_url, 'git@bitbucket.org:loudcloud/myeo2016-rails.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/home/intelimina/myeo2016-rails'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml', 'config/environments/production.rb')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

namespace :deploy do

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
      execute :sudo, "service myeo2016-rails stop"
      execute :sudo, "service myeo2016-rails start"
    end
  end

end

namespace :db do
  task :seed do
    on roles(:db) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "db:seed"
        end
      end
    end
  end

  task :migrate do
    on roles(:db) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "db:migrate"
        end
      end
    end
  end
end

namespace :service do
  task :restart do
    on roles(:app) do
      execute :sudo, "service myeo2016-rails stop"
      execute :sudo, "service myeo2016-rails start"
    end
  end

  task :start do
    on roles(:app) do
      execute :sudo, "service myeo2016-rails start"
    end
  end

  task :stop do
    on roles(:app) do
      execute :sudo, "service myeo2016-rails stop"
    end
  end
end
