class Participant < ActiveRecord::Base
  belongs_to :member
  validates_presence_of :member
  validates_presence_of :email, :last_name, :first_name
end
