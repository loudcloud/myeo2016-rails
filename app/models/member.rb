class Member < ActiveRecord::Base
  has_many :participants, inverse_of: :member
  accepts_nested_attributes_for :participants, allow_destroy: true

  validates_presence_of :email, :last_name, :first_name, :member_number, :contact_number, :payment_option
  validates :i_agree, acceptance: { accept: true }

  after_create :email_notifications

private
  def email_notifications
    RegistrationMailer.success(email).deliver_later
    AdminMailer.registration(self.id).deliver_later
    return true
  end
end
