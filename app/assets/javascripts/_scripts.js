$(function() {
    function display_participant_name() {
        var $this = $(this);
        var $display = $this.parents(".participant__fields").find(".js-participant-display-name");
        var first_name = $display.data('first-name');
        var last_name = $display.data('last-name');

        if($this.hasClass('js-participant-last-name')) {
            last_name = $this.val();
        } else {
            first_name = $this.val();
        }

        $display.html(first_name + " " + last_name);
        $display.data('first-name', first_name);
        $display.data('last-name', last_name);
    }



    if ($('.faq__button')) {
        $('.faq__button').on('click', function(e){
            var $selectedFaqContent = $(e.target).parents('.faq__block-list-item').find('.faq__content');
            var currentlyActive = $selectedFaqContent.hasClass('active')
        $('.faq__content').removeClass('active'); //remove all active
            // Toggle active class
            currentlyActive ?
              $selectedFaqContent.removeClass('active')
              : $selectedFaqContent.addClass('active');
        })
    }

    if ($('.registration__form--button')) {
        // $('.registration__form--button').on('click', function(e) {
        //   e.preventDefault();
        //   if (!e.target.dataset.target && e.target.dataset.target.length > 0) return;
        //   var isReset = e.target.dataset.target === 'reset';

        //   if (isReset) {
        //     // submit form then clear
        //     $(this).parents('.registration__form')[0].reset();
        //   } else {
        //     $(this).parents('.registration__form').removeClass('isActive');
        //     $(e.target.dataset.target).addClass('isActive');
        //   }
        // })
    }

    if ($('.registration__form')) {
        $('.registration__form').on('submit', function(event) {
            var empty = $(this).find("input, textarea").filter(function() {
                return this.value === "";
            });

            var all_answered = true;
            $(".registration__form input:radio").each(function(){
              var name = $(this).attr("name");
              if( $("input:radio[name='"+name+"']:checked").length == 0 ) {
                all_answered = false;
              }
            });

            if(empty.length || !all_answered) {
                event.preventDefault();
                empty.first().focus();

                if(!empty.length && !all_answered) {
                    alert("Please check all hobbies and shirt sizes.");
                }
                return false;
            } else {
                var participants = $(".js-display-fields", ".participants").length;
                var names = []

                $(".js-participant-display-name", ".participants").each(function() {
                    names.push($(this).html());
                });

                names = names.join(", ");

                if(confirm("Confirm registration of " + participants + " (" + names + ") participant/s?")) {

                } else {
                    event.preventDefault();
                    return false;
                }
            }
        });

        $('.registration__form').on('click', '.js-toggle-fields', function() {
            $(this).parents(".participant__fields").find(".js-display-fields").slideToggle(400, function(){
                var $icon = $(".js-toggle-indicator", $(this).parents(".participant__fields"));
                if($(this).is(":hidden")) {
                    $icon.html("&gt;");
                } else {
                    $icon.html("v");
                }
            });
        });

        $('.registration__form').on('keyup', '.js-participant-last-name', display_participant_name);
        $('.registration__form').on('keyup', '.js-participant-first-name', display_participant_name);
        // $('.registration__form').on('change', '.js-participant-nick-name', display_participant_name);

        $(".js-add-participant").on('click', function(event) {
            event.preventDefault();
            $(".js-display-fields", ".participants").slideUp();

            var content = $(".js-participant-field").html();
            var new_id = new Date().getTime();
            var input_names = /member\[participants_attributes\]\[\d+\]/g;
            var input_ids = /member_participants_attributes_\d+/g;
            content = content.replace(input_names, "member[participants_attributes][" + new_id + "]");
            content = content.replace(input_ids, "member_participants_attributes_" + new_id);

            $(".participants").append(content);
            return false;
        })

        $(".js-toggle-waiver").on('click', function(e) {
            e.preventDefault();
            $('#waiver').slideToggle();
        })

        $('.registration__form').on('click', '.js-delete', function() {
            var name = $(this).parents(".participant__fields").find(".js-participant-display-name").html();
            
            if (confirm("Delete this child ( "+ name +" )?")) {
                if($('.participant__fields', '.registration__form').length > 1) {
                    $(this).parents(".participant__fields").remove();
                }
            };
        })
    }
})