ActiveAdmin.register Participant do
  permit_params do
    [ :member_id, :last_name, :first_name, :nickname, :email,
      :birthdate, :hobbies, :shirt_size, :medical_condition, :diet ]
  end
end
