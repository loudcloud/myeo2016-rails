ActiveAdmin.register Member do
  includes :participants
  permit_params do
    [
      :last_name, :first_name, :member_number, 
      :email, :contact_number, :payment_option, 
      participant_ids: [],
      participants_attributes: [
        :member_id, :last_name, :first_name, :nickname, :email,
        :birthdate, :hobbies, :shirt_size, :medical_condition, :diet,
        :_destroy
      ]
    ]
  end
end