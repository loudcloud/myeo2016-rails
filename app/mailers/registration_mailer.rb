class RegistrationMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.registration_mailer.success.subject
  #
  def success(email)
    mail to: email, subject: "Thank you for registering for MyEONextGen 2016!"
  end
end
