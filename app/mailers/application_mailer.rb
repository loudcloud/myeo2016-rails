class ApplicationMailer < ActionMailer::Base
  default from: "inquiries@myeonextgen.com"
  layout 'mailer'
end
