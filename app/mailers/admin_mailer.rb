class AdminMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.admin_mailer.registration.subject
  #
  def registration(member_id)
    @member = Member.find(member_id)
    @participants = @member.participants
    mail to: "inquiries@myeonextgen.com"
  end
end
