class PagesController < ApplicationController
  def index
  end

  def contact
    @member = Member.new
    @member.participants.build
  end

  def faq
  end

  def itinerary
  end

  def reminders
  end

  def do_register
    @member = Member.new(member_params)

    if @member.save
      render action: :thank_you
    else
      render action: :contact
    end
  end

  private
  def member_params
    parameters = [:last_name, :first_name, :member_number, :email, :contact_number, :payment_option, :i_agree]
    parameters << { participants_attributes: [:first_name, :last_name, :nickname, :birthdate, :email, :medical_condition, :hobbies, :shirt_size, :diet] }
    params.require(:member).permit(parameters)
  end
end
